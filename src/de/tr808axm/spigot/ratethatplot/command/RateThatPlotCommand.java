package de.tr808axm.spigot.ratethatplot.command;

import de.tr808axm.spigot.pluginutils.ChatUtil;
import de.tr808axm.spigot.ratethatplot.Main;
import de.tr808axm.spigot.ratethatplot.Rating;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Handles the /ratethatplot command.
 *
 * /rtp - Help menu
 * /rtp rate <player> <atmosphere> <style> <details> <purpose> <notice> - Rate <player> with the given values.
 * /rtp rating [<player>] - View player's or your rating.
 *
 * Created by tr808axm on 09.07.2016.
 */
public class RateThatPlotCommand implements CommandExecutor {
    private final Main plugin;

    public RateThatPlotCommand(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(args.length >= 1) {
            if(args[0].equalsIgnoreCase("rate")) {
                if (sender.hasPermission("ratethatplot.rate")) {
                    if(args.length >= 7) {
                        OfflinePlayer player = plugin.getOfflinePlayerIfExists(args[1]);
                        if(player == null) {
                            plugin.getChatUtil().send(sender, ChatUtil.VARIABLE + args[1] + ChatUtil.NEGATIVE + " has never joined this server!", false);
                            return true;
                        }
                        short atmosphere, style, details, purpose;
                        try {
                            atmosphere = Short.parseShort(args[2]);
                            if (atmosphere < 0 || atmosphere > 10) {
                                plugin.getChatUtil().send(sender, ChatUtil.VARIABLE + "atmosphere" + ChatUtil.NEGATIVE + " has to be a number between 0 and 10!", false);
                                return true;
                            }
                        } catch (NumberFormatException e) {
                            plugin.getChatUtil().send(sender, ChatUtil.VARIABLE + args[2] + ChatUtil.NEGATIVE + " isn't a valid number!", false);
                            return true;
                        }
                        try {
                            style = Short.parseShort(args[3]);
                            if (style < 0 || style > 10) {
                                plugin.getChatUtil().send(sender, ChatUtil.VARIABLE + "style" + ChatUtil.NEGATIVE + " has to be a number between 0 and 10!", false);
                                return true;
                            }
                        } catch (NumberFormatException e) {
                            plugin.getChatUtil().send(sender, ChatUtil.VARIABLE + args[3] + ChatUtil.NEGATIVE + " isn't a valid number!", false);
                            return true;
                        }
                        try {
                            details = Short.parseShort(args[4]);
                            if (details < 0 || details > 10) {
                                plugin.getChatUtil().send(sender, ChatUtil.VARIABLE + "details" + ChatUtil.NEGATIVE + " has to be a number between 0 and 10!", false);
                                return true;
                            }
                        } catch (NumberFormatException e) {
                            plugin.getChatUtil().send(sender, ChatUtil.VARIABLE + args[4] + ChatUtil.NEGATIVE + " isn't a valid number!", false);
                            return true;
                        }
                        try {
                            purpose = Short.parseShort(args[5]);
                            if (purpose < 0 || purpose > 10) {
                                plugin.getChatUtil().send(sender, ChatUtil.VARIABLE + "purpose" + ChatUtil.NEGATIVE + " has to be a number between 0 and 10!", false);
                                return true;
                            }
                        } catch (NumberFormatException e) {
                            plugin.getChatUtil().send(sender, ChatUtil.VARIABLE + args[5] + ChatUtil.NEGATIVE + " isn't a valid number!", false);
                            return true;
                        }
                        String note = args[6];
                        for(int i = 7; i < args.length; i++) {
                            note += " " + args[i];
                        }

                        Rating rating = new Rating(atmosphere, style, details, purpose, note);
                        plugin.saveRating(player.getUniqueId(), rating);
                        plugin.getChatUtil().send(sender, "You successfully rated " + ChatUtil.VARIABLE + player.getName() + ChatUtil.POSITIVE + "!", true);
                        return true;
                    }
                    plugin.getChatUtil().send(sender, "Usage: /ratethatplot rate <playername> <atmosphere (0 - 10)> <style (0 - 10)> <details (0 - 10)> <purpose (0 - 10)> <note (string)>", false);
                    return true;
                } else {
                    plugin.getChatUtil().send(sender, ChatUtil.NO_PERMISSION_MESSAGE, false);
                    return true;
                }
            } else if(args[0].equalsIgnoreCase("rating")) {
                if (sender.hasPermission("ratethatplot.viewrating.own")) {
                    if(args.length >= 2 && sender.hasPermission("ratethatplot.viewrating.other")) {
                        OfflinePlayer player = plugin.getOfflinePlayerIfExists(args[1]);
                        if(player == null) {
                            plugin.getChatUtil().send(sender, ChatUtil.VARIABLE + args[1] + ChatUtil.NEGATIVE + " has never joined this server!", false);
                            return true;
                        }
                        Rating rating = plugin.getRating(player.getUniqueId());
                        if(rating == null) {
                            plugin.getChatUtil().send(sender, "There is no rating for " + ChatUtil.VARIABLE + player.getName() + ChatUtil.NEGATIVE + "!", false);
                            return true;
                        } else {
                            plugin.getChatUtil().send(sender, rating.asChatStrings(player.getName()), true);
                            return true;
                        }
                    } else {
                        if(sender instanceof Player) {
                            Rating rating = plugin.getRating(((Player) sender).getUniqueId());
                            if(rating == null) {
                                plugin.getChatUtil().send(sender, "There is no rating for you!", false);
                                return true;
                            } else {
                                plugin.getChatUtil().send(sender, rating.asChatStrings(sender.getName()), true);
                                return true;
                            }
                        } else {
                            plugin.getChatUtil().send(sender, "You cannot get your own rating as console, please use " + ChatUtil.VARIABLE + "/ratethatplot rating <playername>" + ChatUtil.NEGATIVE + "!", false);
                            return true;
                        }
                    }
                } else {
                    plugin.getChatUtil().send(sender, ChatUtil.NO_PERMISSION_MESSAGE, false);
                    return true;
                }
            } else if(args[0].equalsIgnoreCase("listunrated")) {
                if (sender.hasPermission("ratethatplot.listunrated")) {
                    int pageNumber = 0;
                    if(args.length >= 2) {
                        try {
                            pageNumber = Integer.parseInt(args[1]) - 1;
                        } catch (NumberFormatException e) {
                            plugin.getChatUtil().send(sender, ChatUtil.VARIABLE + args[1] + ChatUtil.NEGATIVE + " isn't a valid number.", false);
                            return true;
                        }
                    }

                    String[] chatStrings = new String[10];
                    chatStrings[0] = ChatColor.BOLD + "List of unrated players (page " + (pageNumber + 1) + "):";
                    chatStrings[9] = ChatUtil.VARIABLE + "Unrated" + ChatColor.GRAY + " | " + ChatUtil.POSITIVE + "Rated (but still in cache)";
                    for(int i = 0; i < 8; i++) { // 0 - 7
                        UUID lineUUID = (plugin.getUnratedPlayers().size() > ((8 * pageNumber) + i)) ? plugin.getUnratedPlayers().get((8 * pageNumber) + i) : null; // if index exists, init
                        chatStrings[i + 1] = (lineUUID != null)
                                ? " - " + (plugin.getRating(lineUUID) == null ? ChatUtil.VARIABLE : ChatUtil.POSITIVE) + plugin.getServer().getOfflinePlayer(lineUUID).getName()
                                : "";
                    }

                    plugin.getChatUtil().send(sender, chatStrings, true);
                    return true;
                } else {
                    plugin.getChatUtil().send(sender, ChatUtil.NO_PERMISSION_MESSAGE, false);
                    return true;
                }
            } else if(args[0].equalsIgnoreCase("refreshunrated")) {
                if (sender.hasPermission("ratethatplot.listunrated")) {
                    if(plugin.searchUnratedPlayers()) {
                        plugin.getChatUtil().send(sender, "Unrated players list updated successfully!", true);
                    } else {
                        plugin.getChatUtil().send(sender, "An exception occurred while refreshing unrated player list.", false);
                    }
                    return true;
                } else {
                    plugin.getChatUtil().send(sender, ChatUtil.NO_PERMISSION_MESSAGE, false);
                    return true;
                }
            }
            plugin.getChatUtil().send(sender, ChatUtil.INVALID_ARGUMENTS_MESSAGE, false);
            return true;
        }

        plugin.getChatUtil().send(sender, "Commands: ", true);
        plugin.getChatUtil().send(sender, "Rate a plot: " + ChatUtil.VARIABLE + "/ratethatplot rate <playername> <atmosphere (0 - 10)> <style (0 - 10)> <details (0 - 10)> <purpose (0 - 10)> <note (string)>", true);
        plugin.getChatUtil().send(sender, "View your rating: " + ChatUtil.VARIABLE + "/ratethatplot rating [<playername>]", true);
        plugin.getChatUtil().send(sender, "List unrated players: " + ChatUtil.VARIABLE + "/ratethatplot listunrated [<pagenumber>]", true);
        plugin.getChatUtil().send(sender, "Refresh unrated-player-list (may produce lag, use with caution): " + ChatUtil.VARIABLE + "/ratethatplot refreshunrated", true);
        return true;
    }

    private boolean isValidNumber(int i) {
        return (i < 0 || i > 10);
    }
}
