package de.tr808axm.spigot.ratethatplot;

import de.tr808axm.spigot.pluginutils.ChatUtil;
import org.bukkit.ChatColor;

/**
 * Used to store a rating.
 * Created by tr808axm on 11.07.2016.
 */
public class Rating {
    private final short atmosphereScore;
    private final short styleScore;
    private final short detailsScore;
    private final short purposeScore;

    private final String note;

    public Rating(short atmosphereScore, short styleScore, short detailsScore, short purposeScore, String note) {
        validateScore(atmosphereScore);
        validateScore(styleScore);
        validateScore(detailsScore);
        validateScore(purposeScore);
        if(note == null) throw new IllegalArgumentException("note may not be null!");

        this.atmosphereScore = atmosphereScore;
        this.styleScore = styleScore;
        this.detailsScore = detailsScore;
        this.purposeScore = purposeScore;
        this.note = note;
    }

    public short getAtmosphereScore() {
        return atmosphereScore;
    }

    public short getStyleScore() {
        return styleScore;
    }

    public short getDetailsScore() {
        return detailsScore;
    }

    public short getPurposeScore() {
        return purposeScore;
    }

    public String getNote() {
        return note;
    }

    private static void validateScore(short score) {
        if(score < 0 || score > 10) throw new IllegalArgumentException("score has to be a number between 0 and 10");
    }

    public String[] asChatStrings(String playername) {
        String[] chatStrings = new String[6];
        chatStrings[0] = ChatColor.BOLD + playername + "'s rating:";
        chatStrings[1] = "Atmosphere score: " + ChatUtil.VARIABLE + atmosphereScore;
        chatStrings[2] = "Style score: " + ChatUtil.VARIABLE + styleScore;
        chatStrings[3] = "Details score: " + ChatUtil.VARIABLE + detailsScore;
        chatStrings[4] = "Purpose score: " + ChatUtil.VARIABLE + purposeScore;
        chatStrings[5] = "Personal note: " + ChatUtil.VARIABLE + note;
        return chatStrings;
    }
}
